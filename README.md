# ProgrammingAssignmentGinmon

Programming Assignment in which we consume GitHub rest end point and do some logic on results

## Getting Started

No installation is required 

### Prerequisites

Before running the project you should import the database which is present in the githubreposdump.sql file
configure the application.properties files according to your machine and configurations (database name, server port...)

## Running the tests

Just use mvn clean install to build the project and run tests

## Deployment

To run the project you should use  mvn spring-boot:run
There are three main features in this application 
	1- Import user into database from gitlab using this link: http://applicationurl/import/{githubusername}
	2- Show programming languages usage percentage from one account using this link: http://applicationurl/show/{githubusername}
	3- List different users from database using this link: http://applicationurl/users/list  

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Malek Attia**

