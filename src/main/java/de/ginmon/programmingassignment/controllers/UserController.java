package de.ginmon.programmingassignment.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import de.ginmon.programmingassignment.entities.User;
import de.ginmon.programmingassignment.exceptions.NoLanguagesFoundException;
import de.ginmon.programmingassignment.exceptions.NoUserFoundException;
import de.ginmon.programmingassignment.services.IUserService;

@Controller
public class UserController {
	@Autowired
	private IUserService userService;

	@GetMapping("import/{login}")
	public ResponseEntity<User> importUserByLogin(
			@PathVariable("login") String login) {
		User user = userService.getUserAndReposFromGitHubRest(login);
		if (user == null) {
			throw new NoUserFoundException();
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@GetMapping("show/{login}")
	public ResponseEntity<Map<String, Double>> showUserLanguagesWeight(
			@PathVariable("login") String login) {
		Map<String, Double> weightByLanguage = userService
				.getUserLanguagesWeight(login);
		if (weightByLanguage == null) {
			throw new NoUserFoundException();
		}
		else if (weightByLanguage.isEmpty()){
			throw new NoLanguagesFoundException();
		}
		return new ResponseEntity<Map<String, Double>>(weightByLanguage,
				HttpStatus.OK);
	}

	@GetMapping("users/list")
	public ResponseEntity<List<User>> showLocalUsersList() {
		List<User> users = userService.getAllUsers();
		if (users == null || users.isEmpty()) {
			return new ResponseEntity<List<User>>(new ArrayList<>(),HttpStatus.OK);
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}

}
