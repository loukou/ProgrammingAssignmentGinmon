package de.ginmon.programmingassignment.services;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import de.ginmon.programmingassignment.entities.User;

public interface IUserService extends Serializable {
	List<User> getAllUsers();

	User getUserByLogin(String login);

	void addUser(User user);

	void updateUser(User user);

	void deleteUser(String login);

	boolean UserExists(String login);

	User getUserAndReposFromGitHubRest(String login);

	Map<String, Double> getUserLanguagesWeight(String login);
}
