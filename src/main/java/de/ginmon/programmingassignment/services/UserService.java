package de.ginmon.programmingassignment.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.LongAdder;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import de.ginmon.programmingassignment.dao.IUserDAO;
import de.ginmon.programmingassignment.entities.Repos;
import de.ginmon.programmingassignment.entities.User;
import de.ginmon.programmingassignment.utils.GitHubRestEndPoints;

@Service
public class UserService implements IUserService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private IUserDAO userDAO;

	@Override
	public List<User> getAllUsers() {
		return userDAO.getAllUsers();
	}

	@Override
	public User getUserByLogin(String login) {
		return userDAO.getUserByLogin(login);
	}

	@Override
	public void addUser(User user) {
		userDAO.addUser(user);

	}

	@Override
	public void updateUser(User user) {
		userDAO.updateUser(user);

	}

	@Override
	public void deleteUser(String login) {
		userDAO.deleteUser(login);

	}

	@Override
	public boolean UserExists(String login) {
		return userDAO.UserExists(login);
	}

	@Override
	public User getUserAndReposFromGitHubRest(String login) {
		User user = getGitHubUserByLogin(login);
		if (user != null) {
			List<Repos> repos = getGitHubReposByLogin(login);
			repos = fillReposLanguages(repos, login);
			for (Repos repo : repos) {
				repo.setOwner(user);
			}
			user.setRepos(repos);
			if (UserExists(login)) {
				updateUser(user);
			} else {
				addUser(user);
			}
			return user;
		}
		return null;
	}

	private User getGitHubUserByLogin(String login) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		try {
			ResponseEntity<User> userResponseEntity = restTemplate.exchange(
					GitHubRestEndPoints.USER, HttpMethod.GET, requestEntity,
					User.class, login);
			User user = userResponseEntity.getBody();
			return user;

		} catch (RestClientException e) {
			Logger.getLogger(UserService.class.getName()).log(Level.WARNING,
					"No user found with login = " + login);
			return null;
		}
	}

	private List<Repos> getGitHubReposByLogin(String login) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<Repos[]> reposResponseEntity = restTemplate.exchange(
				GitHubRestEndPoints.USER_REPOS, HttpMethod.GET, requestEntity,
				Repos[].class, login);
		Repos[] repos = reposResponseEntity.getBody();
		List<Repos> reposList = Arrays.asList(repos);
		return reposList;

	}

	private List<Repos> fillReposLanguages(List<Repos> repos, String login) {
		for (Repos repo : repos) {
			repo.setWeightByLanguage(getGitHubLanguagesByRepos(repo, login));
		}
		return repos;
	}

	private Map<String, Integer> getGitHubLanguagesByRepos(Repos repos,
			String login) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<Object> reposResponseEntity = restTemplate.exchange(
				GitHubRestEndPoints.USER_REPOS_LANGUAGES, HttpMethod.GET,
				requestEntity, Object.class, login, repos.getName());
		Object object = reposResponseEntity.getBody();
		@SuppressWarnings("unchecked")
		Map<String, Integer> weightByLanguage = (Map<String, Integer>) object;
		return weightByLanguage;
	}

	@Override
	public Map<String, Double> getUserLanguagesWeight(String login) {
		User user = getUserByLogin(login);
		if (user != null) {
			Map<String, Integer> sizeByLanguage = new HashMap<>();
			for (Repos repos : user.getRepos()) {
				for (String language : repos.getWeightByLanguage().keySet()) {
					if (sizeByLanguage.get(language) == null) {
						sizeByLanguage.put(language, repos
								.getWeightByLanguage().get(language));
					} else {
						sizeByLanguage.put(
								language,
								sizeByLanguage.get(language)
										+ repos.getWeightByLanguage().get(
												language));
					}
				}
			}
			if (!sizeByLanguage.isEmpty()) {
				LongAdder adder = new LongAdder();
				sizeByLanguage.values().stream().parallel().forEach(adder::add);
				long totalDataSize = adder.longValue();
				return getPercentageFromMap(sizeByLanguage, totalDataSize);
			} else {
				return new HashMap<>();
			}

		}
		return null;
	}

	private Map<String, Double> getPercentageFromMap(
			Map<String, Integer> sizeByLanguage, long totalDataSize) {
		Map<String, Double> resultMap = new HashMap<>();
		for (String language : sizeByLanguage.keySet()) {
			resultMap.put(language,
					((double) sizeByLanguage.get(language) / totalDataSize));
		}
		return resultMap;

	}

}
