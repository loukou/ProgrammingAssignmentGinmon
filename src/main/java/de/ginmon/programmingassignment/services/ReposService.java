package de.ginmon.programmingassignment.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.ginmon.programmingassignment.dao.IRepoDAO;
import de.ginmon.programmingassignment.entities.Repos;
import de.ginmon.programmingassignment.entities.User;

@Service
public class ReposService implements IReposService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private IRepoDAO repoDAO;

	@Override
	public List<Repos> getAllReposs() {
		return repoDAO.getAllReposs();
	}

	@Override
	public List<Repos> getReposByUser(User user) {
		return repoDAO.getReposByUser(user);
	}

	@Override
	public Repos getReposByName(String name) {
		return repoDAO.getReposByName(name);
	}

	@Override
	public void addRepos(Repos repos) {
		repoDAO.addRepos(repos);

	}

	@Override
	public void updateRepos(Repos repos) {
		repoDAO.updateRepos(repos);

	}

	@Override
	public void deleteRepos(String name) {
		repoDAO.deleteRepos(name);

	}

	@Override
	public boolean ReposExists(String name) {
		return repoDAO.ReposExists(name);
	}

}
