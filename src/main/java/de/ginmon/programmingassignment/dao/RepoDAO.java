package de.ginmon.programmingassignment.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.ginmon.programmingassignment.entities.Repos;
import de.ginmon.programmingassignment.entities.User;

@Transactional
@Repository
public class RepoDAO implements IRepoDAO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Repos> getAllReposs() {
		String hql = "FROM Repos as repo";
		return (List<Repos>) entityManager.createQuery(hql).getResultList();
	}

	@Override
	public Repos getReposByName(String name) {
		Query query = entityManager.createQuery(
				"Select repo FROM Repos repo WHERE repo.name =:name",
				Repos.class);
		query.setParameter("name", name);
		@SuppressWarnings("unchecked")
		List<Repos> repos = query.getResultList();
		if (repos != null && repos.size() > 0) {
			return repos.get(0);
		}
		return null;
	}

	@Override
	public void addRepos(Repos repos) {
		entityManager.persist(repos);
	}

	@Override
	public void updateRepos(Repos repos) {
		entityManager.merge(repos);

	}

	@Override
	public void deleteRepos(String name) {
		entityManager.remove(getReposByName(name));

	}

	@Override
	public boolean ReposExists(String name) {
		Repos repos = getReposByName(name);
		if (repos != null) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Repos> getReposByUser(User user) {
		Query query = entityManager
				.createQuery("Select repo FROM Repos repo WHERE repo.user =:user");
		query.setParameter("user", user);
		return query.getResultList();
	}

}
