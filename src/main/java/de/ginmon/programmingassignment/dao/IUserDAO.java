package de.ginmon.programmingassignment.dao;

import java.io.Serializable;
import java.util.List;

import de.ginmon.programmingassignment.entities.User;

public interface IUserDAO extends Serializable{
	List<User> getAllUsers();

	User getUserByLogin(String login);

	void addUser(User user);

	void updateUser(User user);

	void deleteUser(String login);

	boolean UserExists(String login);
}
