package de.ginmon.programmingassignment.dao;

import java.io.Serializable;
import java.util.List;

import de.ginmon.programmingassignment.entities.Repos;
import de.ginmon.programmingassignment.entities.User;

public interface IRepoDAO extends Serializable{
	List<Repos> getAllReposs();
	
	List<Repos> getReposByUser(User user);

	Repos getReposByName(String name);

	void addRepos(Repos repos);

	void updateRepos(Repos repos);

	void deleteRepos(String name);

	boolean ReposExists(String name);
	
}
