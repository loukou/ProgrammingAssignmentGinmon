package de.ginmon.programmingassignment.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.ginmon.programmingassignment.entities.User;

@Transactional
@Repository
public class UserDAO implements IUserDAO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUsers() {
		String hql = "FROM User as usr";
		return (List<User>) entityManager.createQuery(hql).getResultList();
	}

	@Override
	public User getUserByLogin(String login) {
		Query query = entityManager.createQuery(
				"Select usr FROM User usr WHERE usr.login =:login", User.class);
		query.setParameter("login", login);
		@SuppressWarnings("unchecked")
		List<User> users = query.getResultList();
		if (users != null && users.size() > 0) {
			return users.get(0);
		}
		return null;
	}

	@Override
	public void addUser(User user) {
		entityManager.persist(user);

	}

	@Override
	public void updateUser(User user) {
		entityManager.merge(user);

	}

	@Override
	public void deleteUser(String login) {
		entityManager.remove(getUserByLogin(login));

	}

	@Override
	public boolean UserExists(String login) {
		User user = getUserByLogin(login);
		if (user != null) {
			return true;
		}
		return false;
	}
}
