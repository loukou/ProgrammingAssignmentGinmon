package de.ginmon.programmingassignment.entities;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Repos {

	@Id
	private int id;
	@Column(name = "name", unique = true)
	private String name;
	private String url;
	@JsonIgnore
	@ManyToOne
	private User owner;
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection
	private Map<String, Integer> weightByLanguage;

	public Repos() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Map<String, Integer> getWeightByLanguage() {
		return weightByLanguage;
	}

	public void setWeightByLanguage(
			Map<String, Integer> weightByLanguage) {
		this.weightByLanguage = weightByLanguage;
	}
}
