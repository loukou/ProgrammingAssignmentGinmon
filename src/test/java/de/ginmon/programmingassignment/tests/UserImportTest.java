package de.ginmon.programmingassignment.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import de.ginmon.programmingassignment.services.IUserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserImportTest {
	
	@Autowired
	IUserService userService;
	
	@Test
	public void testNoFoundUser(){
		String userName = "impossibleusernameexist";
		assertThat(userService.getUserByLogin(userName)).isNull();
	}
	@Test
	public void testUserExists(){
		String userName = "ginmon";
		assertThat(userService.getUserAndReposFromGitHubRest(userName)).isNotNull();
	}
	@Test
	public void testRepoIsEmpty(){
		String userName = "loukou007";
		assertThat(userService.getUserLanguagesWeight(userName)).isEmpty();
	}
	@Test
	public void testRepoIsNotEmpty(){
		String userName = "ginmon";
		assertThat(userService.getUserLanguagesWeight(userName)).isNotEmpty();
	}
}
