-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 26 Juin 2017 à 01:19
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `githubrepos`
--

-- --------------------------------------------------------

--
-- Structure de la table `repos`
--

CREATE TABLE IF NOT EXISTS `repos` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2r5pk7o4uy3viqj3o9u6453ch` (`name`),
  KEY `FK743bqvvm2i0j4plpk71a1s8q1` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `repos`
--

INSERT INTO `repos` (`id`, `name`, `url`, `owner_id`) VALUES
(70056664, 'react-native-ios-charts', 'https://api.github.com/repos/ginmon/react-native-ios-charts', 9991806),
(72095490, 'Charts', 'https://api.github.com/repos/ginmon/Charts', 9991806),
(81081302, 'node-pdf-generator', 'https://api.github.com/repos/ginmon/node-pdf-generator', 9991806),
(81345454, 'angular2-i18next', 'https://api.github.com/repos/ginmon/angular2-i18next', 9991806);

-- --------------------------------------------------------

--
-- Structure de la table `repos_weight_by_language`
--

CREATE TABLE IF NOT EXISTS `repos_weight_by_language` (
  `repos_id` int(11) NOT NULL,
  `weight_by_language` int(11) DEFAULT NULL,
  `weight_by_language_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`repos_id`,`weight_by_language_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `repos_weight_by_language`
--

INSERT INTO `repos_weight_by_language` (`repos_id`, `weight_by_language`, `weight_by_language_key`) VALUES
(70056664, 32517, 'JavaScript'),
(70056664, 5511, 'Objective-C'),
(70056664, 123980, 'Swift'),
(72095490, 252171, 'Objective-C'),
(72095490, 5946, 'Ruby'),
(72095490, 1312, 'Shell'),
(72095490, 875230, 'Swift'),
(81081302, 1172, 'JavaScript'),
(81345454, 1977, 'CSS'),
(81345454, 2489, 'HTML'),
(81345454, 8196, 'JavaScript'),
(81345454, 1052, 'Nginx'),
(81345454, 107754, 'TypeScript');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ew1hvam8uwaknuaellwhqchhb` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `login`, `name`, `url`) VALUES
(9991806, 'ginmon', 'Ginmon GmbH', 'https://api.github.com/users/ginmon'),
(29372722, 'loukou007', NULL, 'https://api.github.com/users/loukou007');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `repos`
--
ALTER TABLE `repos`
  ADD CONSTRAINT `FK743bqvvm2i0j4plpk71a1s8q1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `repos_weight_by_language`
--
ALTER TABLE `repos_weight_by_language`
  ADD CONSTRAINT `FKjd32am78cvt2x0s5v79m5kwpc` FOREIGN KEY (`repos_id`) REFERENCES `repos` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
